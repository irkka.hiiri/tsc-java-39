package ru.tsc.ichaplygina.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static java.lang.System.currentTimeMillis;


@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractModel implements Cloneable {

    @Nullable
    private String signature;
    private long timeStamp = currentTimeMillis();
    @NotNull
    private String userId;

    public Session(@NotNull String userId) {
        this.userId = userId;
    }

    @Override
    public Session clone() throws CloneNotSupportedException {
        return (Session) super.clone();
    }

    @Override
    public String toString() {
        return getId() + " " + userId + "" + timeStamp;
    }
}
